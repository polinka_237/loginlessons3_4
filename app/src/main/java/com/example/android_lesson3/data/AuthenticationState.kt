package com.example.android_lesson3.data

data class AuthenticationState(
    val userName: String = "",
    val password: String = "",
    val loading: Boolean = false,
    var togglePasswordVisibility: Boolean = true
) {
    companion object {
        val EMPTY_STATE = AuthenticationState()
    }
}
