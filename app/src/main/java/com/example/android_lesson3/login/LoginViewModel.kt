package com.example.android_lesson3.login

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android_lesson3.data.AuthenticationState
import com.example.android_lesson3.helperclasses.MutableSavedState
import com.example.android_lesson3.helperclasses.ProgressLoader
import com.example.android_lesson3.helperclasses.SampleLoginDispatchers
import com.example.android_lesson3.helperclasses.combineFlows
import com.example.android_lesson3.helperclasses.stateIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CloseableCoroutineDispatcher
import kotlinx.coroutines.plus
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    stateHandle: SavedStateHandle,
    dispatchers: SampleLoginDispatchers
) : ViewModel() {
    private val username = MutableSavedState(
        stateHandle,
    "UserName",
        defValue = ""
    )
    private val password = MutableSavedState(
        stateHandle,
        "password",
        defValue = ""
    )
    private val passwordVisibility = MutableSavedState(
        stateHandle,
        "password_key",
        defValue = false
    )
    private val loadingProgress = ProgressLoader()


    fun userNameChanged(userName: String) {
        username.value = userName
    }

    val state = combineFlows(
        username.flow,
        password.flow,
        passwordVisibility.flow,
        loadingProgress.flow
    ) { username, password, passwordToggle, isLoading ->
        AuthenticationState(
            userName = username,
            password = password,
            togglePasswordVisibility = passwordToggle,
            loading = isLoading
        )
    }.stateIn(
        coroutineScope = viewModelScope + dispatchers.main,
        initialValue = AuthenticationState.EMPTY_STATE
    )
}